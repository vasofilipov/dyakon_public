(function() {
  function Item(id, description) {
    this.id = id;
    this.description = description;
  }

  window.app.ENGINE.Item = Item;

})();
