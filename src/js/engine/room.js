(function() {
  function Room(type, size) {
    this.agents = {};
    this.items = {};
    this.type = type;
    this.size = size;
  }

  Room.prototype.look = function(command) {
    var description = "You are in a " + this.size + " " + this.type + ".";

    if (Object.getOwnPropertyNames(this.agents).length) {
      var agents = "There's a " + this.agents['dead'].status + " " + this.agents['dead'].type + " " + this.agents['dead'].location + ".";
    }
    window.app.ENGINE.playCutscene(command, $(".game-screen"));
    window.app.ENGINE.playCutscene(description, $(".game-screen"));
    window.app.ENGINE.playCutscene(agents, $(".game-screen"));
  }

  Room.prototype.addAgent = function(agentID) {
    this.agents[agentID] = window.WORLD.agents[agentID];
  }

  window.app.ENGINE.Room = Room;

})();
