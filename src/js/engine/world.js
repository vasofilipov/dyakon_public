(function() {

  function World () {
    this.creationTime = (new Date).getTime();
    this.difficulty = "easy";
    this.agents = {};
    this.hero = null;
    this.curRoom = null;
  }

  World.prototype.getCreationTime = function() {
    return this.creationTime;
  };

  World.prototype.registerAgent = function(agent) {
    var id = agent.id;
    if (!this.agents[id]) {
      this.agents[id] = agent;
    } else {
      throw "Existing agent id.";
    }
  }

  World.prototype.executeLook = function(command) {
    this.curRoom.look(command)
  }

  window.app.ENGINE.World = World;

})();
