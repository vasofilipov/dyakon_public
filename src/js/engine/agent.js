(function() {
  function BaseAgent() {
    this.inventory = {};
  };

  BaseAgent.prototype.addItem2Inv = function(item) {
    this.inventory[item.id] = item;
  }

  function Agent(id, WD, isHero) {
    this.id = id;
    this.status = "tall";
    this.type = "man";
    this.location = "near the window"
    WD.registerAgent(this);
    if (isHero && !WD.hero) {
      WD.hero = this;
    }
  };

  Agent.prototype = new BaseAgent();

  Agent.prototype.constructor=Agent;

  Agent.prototype.setStatus = function(status) {
    this.status = status;
    return this;
  };

  Agent.prototype.setType = function(type) {
    this.type = type;
    return this;
  };

  Agent.prototype.setLocation = function(location) {
    this.location = location;
    return this;
  };
  

  window.app.ENGINE.Agent = Agent;

})();
