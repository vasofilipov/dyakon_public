(function() {

  function initTextInput(interpr) {
    $(".game-input")
      .on("execute", function() {interpretCommand(interpr)})
      .on("keyup", inputEnter);
  }

  function interpretCommand(interpr) {
    var command = $(".game-input").val();
    interpr.exec(command);
  }

  function inputEnter(e) {
    if (e.keyCode == 13) {
      $(this).trigger("execute");
    }
  }

  function initInterpreter(interpr) {
    initTextInput(interpr);
  }


  function Interpret(WD) {
      this.WORLD = WD;
  }

  Interpret.prototype.exec = function(command) {
    var command = command.toUpperCase().trim();
    var func = this.COMMANDS(command);
    if (func) {
      func.call(this.WORLD, command);
    } else {
      this.unrecognized(command);
    }
  }

  Interpret.prototype.COMMANDS = function(command) {
    var json = {
      "LOOK": this.WORLD.executeLook,
      "LOOK AROUND": this.WORLD.executeLook
    }
    return json[command]
  }

  Interpret.prototype.unrecognized = function(command) {
    app.ENGINE.playCutscene("Unrecognized command '" + command + "'", $(".game-screen"));
  }

  window.app.INTRP.initInterpreter = initInterpreter;
  window.app.INTRP.Interpret = Interpret;

})();
