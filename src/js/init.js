$(function() {
  window.WORLD = window.WORLD || new window.app.ENGINE.World();
  window.INTERPRETER = window.INTERPRETER|| new window.app.INTRP.Interpret(WORLD);

  function createHeroWithSword() {
    var hero = new app.ENGINE.Agent("hero", WORLD, true);
    var sword = new app.ENGINE.Item("Sword", "Elven Sword");
    var room1 = new app.ENGINE.Room("room", "small and cold");
    var room2 = new app.ENGINE.Room("corridor", "dark");
    var deadMan = new app.ENGINE.Agent("dead", WORLD, true);
    WORLD.curRoom = room1;
    deadMan.setStatus("dead").setType("man").setLocation("on the ground");
    room1.addAgent("dead");

    hero.addItem2Inv(sword);

    $.getJSON("src/jsons/intro.json", function(data) {
      var intro = data.text;
      app.ENGINE.playCutscene(intro, $(".game-screen"));
      app.INTRP.initInterpreter(INTERPRETER);
    });
  }

  createHeroWithSword();

});
