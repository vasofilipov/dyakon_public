(function() {

  function playCutscene(text, $container) {
    if (!text) {
      return;
    }
    var write = true,
        tag = "",
        coef = 1,
        loop = true;
    if (!WORLD.busy) {
      writeText(text, $container, write, tag, coef);
    } else {
      var interval = setInterval(function() {
        if (!WORLD.busy) {
          clearInterval(interval);
          writeText(text, $container, write, tag, coef);
        }
      }, 400);
    }
  }

  function writeText(text, $container, write, tag, coef) {
    WORLD.busy = true;
    text.split("").forEach(function(letter, idx) {
      coef = letter.length == 1 ? 1 : 2;
      if (letter == "<") {
        write = false;
      }

      if (letter == ">") {
        write = true;
        tag += ">";
        letter = tag;
      }

      if (write) {
        setTimeout(function() {
          console.log(idx + 1, text.length)
          if (idx + 1 >= text.length) {
            WORLD.busy = false;
          }
          $container.append(letter);
          $container[0].scrollTop = $container[0].scrollHeight
        }, 5 * 1 * idx);

      } else {
        tag += letter;
      }

    });
    $container.append("<br>");
  }

  app.ENGINE.playCutscene = playCutscene;

})();
