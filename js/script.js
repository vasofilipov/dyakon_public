// @koala-append "engine/world.js"
// @koala-append "engine/item.js"
// @koala-append "engine/agent.js"
// @koala-append "story/cutscene.js"
// @koala-append "engine/room.js"
// @koala-append "interface/interpreter.js"
// @koala-append "init.js"


(function() {
  window.app = {
    ENGINE: {},
    INTRP: {}
  };

})();


(function() {

  function World () {
    this.creationTime = (new Date).getTime();
    this.difficulty = "easy";
    this.agents = {};
    this.hero = null;
    this.curRoom = null;
  }

  World.prototype.getCreationTime = function() {
    return this.creationTime;
  };

  World.prototype.registerAgent = function(agent) {
    var id = agent.id;
    if (!this.agents[id]) {
      this.agents[id] = agent;
    } else {
      throw "Existing agent id.";
    }
  }

  World.prototype.executeLook = function(command) {
    this.curRoom.look(command)
  }

  window.app.ENGINE.World = World;

})();


(function() {
  function Item(id, description) {
    this.id = id;
    this.description = description;
  }

  window.app.ENGINE.Item = Item;

})();


(function() {
  function BaseAgent() {
    this.inventory = {};
  };

  BaseAgent.prototype.addItem2Inv = function(item) {
    this.inventory[item.id] = item;
  }

  function Agent(id, WD, isHero) {
    this.id = id;
    this.status = "tall";
    this.type = "man";
    this.location = "near the window"
    WD.registerAgent(this);
    if (isHero && !WD.hero) {
      WD.hero = this;
    }
  };

  Agent.prototype = new BaseAgent();

  Agent.prototype.constructor=Agent;

  Agent.prototype.setStatus = function(status) {
    this.status = status;
    return this;
  };

  Agent.prototype.setType = function(type) {
    this.type = type;
    return this;
  };

  Agent.prototype.setLocation = function(location) {
    this.location = location;
    return this;
  };
  

  window.app.ENGINE.Agent = Agent;

})();


(function() {

  function playCutscene(text, $container) {
    if (!text) {
      return;
    }
    var write = true,
        tag = "",
        coef = 1,
        loop = true;
    if (!WORLD.busy) {
      writeText(text, $container, write, tag, coef);
    } else {
      var interval = setInterval(function() {
        if (!WORLD.busy) {
          clearInterval(interval);
          writeText(text, $container, write, tag, coef);
        }
      }, 400);
    }
  }

  function writeText(text, $container, write, tag, coef) {
    WORLD.busy = true;
    text.split("").forEach(function(letter, idx) {
      coef = letter.length == 1 ? 1 : 2;
      if (letter == "<") {
        write = false;
      }

      if (letter == ">") {
        write = true;
        tag += ">";
        letter = tag;
      }

      if (write) {
        setTimeout(function() {
          console.log(idx + 1, text.length)
          if (idx + 1 >= text.length) {
            WORLD.busy = false;
          }
          $container.append(letter);
          $container[0].scrollTop = $container[0].scrollHeight
        }, 5 * 1 * idx);

      } else {
        tag += letter;
      }

    });
    $container.append("<br>");
  }

  app.ENGINE.playCutscene = playCutscene;

})();


(function() {
  function Room(type, size) {
    this.agents = {};
    this.items = {};
    this.type = type;
    this.size = size;
  }

  Room.prototype.look = function(command) {
    var description = "You are in a " + this.size + " " + this.type + ".";

    if (Object.getOwnPropertyNames(this.agents).length) {
      var agents = "There's a " + this.agents['dead'].status + " " + this.agents['dead'].type + " " + this.agents['dead'].location + ".";
    }
    window.app.ENGINE.playCutscene(command, $(".game-screen"));
    window.app.ENGINE.playCutscene(description, $(".game-screen"));
    window.app.ENGINE.playCutscene(agents, $(".game-screen"));
  }

  Room.prototype.addAgent = function(agentID) {
    this.agents[agentID] = window.WORLD.agents[agentID];
  }

  window.app.ENGINE.Room = Room;

})();


(function() {

  function initTextInput(interpr) {
    $(".game-input")
      .on("execute", function() {interpretCommand(interpr)})
      .on("keyup", inputEnter);
  }

  function interpretCommand(interpr) {
    var command = $(".game-input").val();
    interpr.exec(command);
  }

  function inputEnter(e) {
    if (e.keyCode == 13) {
      $(this).trigger("execute");
    }
  }

  function initInterpreter(interpr) {
    initTextInput(interpr);
  }


  function Interpret(WD) {
      this.WORLD = WD;
  }

  Interpret.prototype.exec = function(command) {
    var command = command.toUpperCase().trim();
    var func = this.COMMANDS(command);
    if (func) {
      func.call(this.WORLD, command);
    } else {
      this.unrecognized(command);
    }
  }

  Interpret.prototype.COMMANDS = function(command) {
    var json = {
      "LOOK": this.WORLD.executeLook,
      "LOOK AROUND": this.WORLD.executeLook
    }
    return json[command]
  }

  Interpret.prototype.unrecognized = function(command) {
    app.ENGINE.playCutscene("Unrecognized command '" + command + "'", $(".game-screen"));
  }

  window.app.INTRP.initInterpreter = initInterpreter;
  window.app.INTRP.Interpret = Interpret;

})();


$(function() {
  window.WORLD = window.WORLD || new window.app.ENGINE.World();
  window.INTERPRETER = window.INTERPRETER|| new window.app.INTRP.Interpret(WORLD);

  function createHeroWithSword() {
    var hero = new app.ENGINE.Agent("hero", WORLD, true);
    var sword = new app.ENGINE.Item("Sword", "Elven Sword");
    var room1 = new app.ENGINE.Room("room", "small and cold");
    var room2 = new app.ENGINE.Room("corridor", "dark");
    var deadMan = new app.ENGINE.Agent("dead", WORLD, true);
    WORLD.curRoom = room1;
    deadMan.setStatus("dead").setType("man").setLocation("on the ground");
    room1.addAgent("dead");

    hero.addItem2Inv(sword);

    $.getJSON("src/jsons/intro.json", function(data) {
      var intro = data.text;
      app.ENGINE.playCutscene(intro, $(".game-screen"));
      app.INTRP.initInterpreter(INTERPRETER);
    });
  }

  createHeroWithSword();

});
